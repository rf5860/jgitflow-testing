* JGit-Flow - Hotfix with Open Release

```bash
mvn jgitflow:release-start # Accept defaults
mvn jgitflow:hotfix-start  # Accept defaults

# Make commits on all branches
for branch in release/1.0 hotfix/1.0.1 master develop; do
echo "${branch}">> ${branch}.out
git add .;git commit -m "On ${branch}";
done

mvn jgitflow:hotfix-finish
```

